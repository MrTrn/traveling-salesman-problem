# Traveling Salesman Problem #
Assignment from DA-OPT3900 Algorithm Optimization course, Buskerud and Vestfold University College, fall 2013. 
Program is in English, but code comments in Norwegian. Read the full assignment further down...

## Technology / topics ##
* Traveling Salesman Problem 
* Random routine
* Greedy routine
* Greedy Heuristic routine
* Double matrices in C#

## Testing the program ##
N = Number of cities.

**N=10, 2 minutes improvement - test run of the program**
![10-2.jpg](https://bitbucket.org/repo/ar8o66/images/2957046608-10-2.jpg)

**N = 500, 10 minutes improvement**
![500-10.jpg](https://bitbucket.org/repo/ar8o66/images/763360581-500-10.jpg)

**N=500, 120 minutes improvement**
![500-120.jpg](https://bitbucket.org/repo/ar8o66/images/2341409733-500-120.jpg)

**N=1000, 120 minutes improvement**
![1000-120.jpg](https://bitbucket.org/repo/ar8o66/images/3879437761-1000-120.jpg)

**N=2000, 120 minutes improvement**
![2000-120.jpg](https://bitbucket.org/repo/ar8o66/images/712069026-2000-120.jpg)

**N=2000, 1800 minutes (30 h) improvement**
![1000-1800.jpg](https://bitbucket.org/repo/ar8o66/images/916954837-1000-1800.jpg)


## Assignment ##
Freely translated from Norwegian to English by Google translate ;)
** The task **
is to solve the Traveling Salesman Problem. You should create a program that do following: 
Generating a graph that consists of 100 cities (n = 100). Each city has an edge with all the cities. 
If n = 100, each city has 99 edges. The distance between the cities should be a number between 1 
and 5. 

* Select two algorithms that can provide an initial solution. For example (random, greedy). 
* Try to improve the initial solution with an improvement method. 
(Greedy method, for example) 
* Test the program with n = 500, 1000, 2000. 
* What is the conclusion?